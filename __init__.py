bl_info = {
    "name": "Custom Shelf 2",
    "author": "Christophe Seux",
    "version": (0, 3),
    "blender": (2, 83, 0),
    "category": "User"
}

from .operators import CSHELF_OT_remove_shelf_dir, CSHELF_OT_add_shelf_dir, \
CSHELF_OT_add_script, CSHELF_OT_set_icon
from .properties import ModeProps, CustomShelfProps, CustomShelfPrefs, ShelveDirProps
from .menus import CSHELF_MT_text_editor
from .constants import *
from .functions import read_shelves

import bpy

classes = [
CSHELF_OT_add_script,
CSHELF_OT_add_shelf_dir,
CSHELF_OT_remove_shelf_dir,
CSHELF_OT_set_icon,
ShelveDirProps,
CustomShelfPrefs,
ModeProps,
CustomShelfProps,
CSHELF_MT_text_editor,
]

def draw_shelf_menu(self, context):
    """draw the menu"""

    layout = self.layout
    #layout.menu(CSHELF_MT_text_editor.bl_idname, icon='ASSET_MANAGER')
    layout.separator()
    layout.operator("customshelf.add_script", text='',
    icon= "ASSET_MANAGER")

def register():
    for cls in classes :
        bpy.utils.register_class(cls)

    read_shelves()

    bpy.types.TEXT_MT_editor_menus.append(draw_shelf_menu)
    bpy.types.WindowManager.custom_shelf = bpy.props.PointerProperty(type=CustomShelfProps)

def unregister():
    del bpy.types.WindowManager.custom_shelf
    bpy.types.TEXT_MT_editor_menus.remove(draw_shelf_menu)

    for cls in classes[::-1] :
        bpy.utils.unregister_class(cls)
