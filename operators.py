import bpy
from bpy.props import IntProperty,BoolProperty, StringProperty, EnumProperty

from pathlib import Path
from os.path import abspath
import json

from .utils import *
from .functions import *
from .properties import CustomShelfProps


class CSHELF_OT_add_shelf_dir(bpy.types.Operator):
    bl_idname = "customshelf.add_shelf_dir"
    bl_label = 'Add Shelf Directory'

    def execute(self, context):
        prefs = bpy.context.preferences.addons[__package__].preferences
        path = prefs.shelf_dirs.add()
        return {'FINISHED'}


class CSHELF_OT_remove_shelf_dir(bpy.types.Operator):
    bl_idname = "customshelf.remove_shelf_dir"
    bl_label = 'Remove Shelf Directory'

    index : IntProperty()

    def execute(self, context):
        prefs = bpy.context.preferences.addons[__package__].preferences
        prefs.shelf_dirs.remove(self.index)

        return {'FINISHED'}


class CSHELF_OT_set_icon(bpy.types.Operator):
    bl_idname = "customshelf.set_icon"
    bl_label = 'Set Shelf Icon'

    icon : StringProperty()

    def execute(self, context):
        props = get_props()
        props.icon = self.icon
        props.show_icons = False

        return {'FINISHED'}


ICONS=[
['SCENE_DATA','RENDERLAYERS','MATERIAL_DATA','GROUP_UVS','TEXTURE','WORLD',
'SPEAKER','TEXT','NODETREE','NODE_INSERT_OFF','PARTICLES','SORTALPHA'],
['MODIFIER','MOD_WAVE','MOD_SUBSURF','MOD_FLUIDSIM','MOD_OCEAN','BLANK1',
'ARMATURE_DATA','BONE_DATA','GROUP_BONE'],
['SEQUENCE','CAMERA_DATA','SCENE','BLANK1',
'FILE_NEW','CONSOLE','BLENDER','APPEND_BLEND'],
['GROUP','MESH_CUBE','MESH_PLANE','MESH_CIRCLE','MESH_UVSPHERE','MESH_GRID',
'EMPTY_DATA','OUTLINER_DATA_MESH', 'LIGHT_SUN','LIGHT_SPOT','LIGHT'],
['TRIA_RIGHT_BAR','REC','PLAY','PREV_KEYFRAME','NEXT_KEYFRAME','PAUSE',
'X','ADD','REMOVE','RESTRICT_VIEW_OFF','RESTRICT_VIEW_ON','RESTRICT_SELECT_OFF',],
['BRUSH_DATA','GREASEPENCIL','LINE_DATA','PARTICLEMODE','SCULPTMODE_HLT',
'WPAINT_HLT','TPAINT_HLT','VIEWZOOM','HAND','KEY_HLT','KEY_DEHLT',],
['PLUGIN','SCRIPT','PREFERENCES',
'ACTION','SOLO_OFF',
'RNDCURVE','SNAP_ON',
'FORCE_WIND','COPY_ID','EYEDROPPER','AUTO','UNLOCKED','LOCKED',
'UNPINNED','PINNED','PLUGIN','HELP','GHOST_ENABLED','GHOST_DISABLED','COLOR',
'LINKED','UNLINKED','LINKED','ZOOM_ALL',
'FREEZE','STYLUS_PRESSURE','FILE_TICK',
'QUIT','RECOVER_LAST','TIME','PREVIEW_RANGE',
'OUTLINER','NLA','EDITMODE_HLT',
'BOIDS','RNA','CAMERA_STEREO'],
]

class CSHELF_OT_add_script(bpy.types.Operator) :
    bl_idname = "customshelf.add_script"
    bl_label = 'Add script to a shelf'

    new_menu : StringProperty()
    add_menu : BoolProperty(default=False)

    name : StringProperty()
    description : StringProperty()

    def get_all_icons (self) :
        ui_layout = bpy.types.UILayout
        icons = ui_layout.bl_rna.functions["prop"].parameters["icon"].enum_items.keys()

        prefixes = ('BRUSH_','MATCAP_','COLORSET_')
        exception = ('BRUSH_DATA')

        return [i for i in icons if not i.startswith(prefixes) or i in exception]

    def draw(self,context) :
        props = get_props()
        layout = self.layout

        row = layout.row(align=True)
        row.prop(props,'show_icons', text='', icon=props.icon)
        row.prop(self,'name',text ='')

        if props.show_icons :
            col = layout.column(align = True)
            for icon_list in ICONS :
                i=0
                for icon in icon_list :
                    if not i%12 :
                        row = col.row(align= True)

                    row.operator('customshelf.set_icon',icon=icon, emboss=False, text='').icon = icon
                    i += 1

                row = col.row(align= True)

        layout.prop(self,'description', text='')
        layout.separator()

        row = layout.row()
        row.prop(props, 'space', text='Space')
        if props.space == 'VIEW3D' :
            row.prop(props.modes, props.space, text='Mode')

        #Shelf Dir Row
        row = layout.row(align = True)
        row.label(text='', icon='FILE_FOLDER')
        row.prop(props, 'shelf_dir', expand=True)

        #Menu Row
        row = layout.row(align = True)
        row.label(text='', icon='MENU_PANEL')
        #row.separator()
        if not self.add_menu :
            row.prop(props,'menu',expand = True)
        else  :
            row.prop(self,'new_menu', text='')

        row.prop(self,'add_menu', icon = 'ADD',text='')

    def execute(self,context) :
        prefs = get_prefs()
        props = get_props()

        if self.new_menu :
            shelf_path = Path(props.shelf_dir)/self.new_menu/f'{norm_name(self.name)}.py'
            shelf_path.parent.mkdir(exist_ok=True)
        else :
            shelf_path = Path(props.shelf_dir)/props.menu/f'{norm_name(self.name)}.py'

        self.info['bl_icon'] = self.icon
        self.info['bl_description'] = self.description
        self.info['bl_space'] = props.space
        self.info['bl_mode'] = getattr(props.modes, props.space)

        shelf_info = json.dumps(self.info, indent=4).replace('"', "'")
        shelf = f'info = {shelf_info}\n\n{self.raw}'

        shelf_path.write_text(shelf)

        line_index = self.active_text.current_line_index
        bpy.data.texts.remove(self.active_text)
        text = bpy.data.texts.load( str(shelf_path) )
        context.space_data.text = text

        text.current_line_index = line_index

        read_shelves()

        return {"FINISHED"}

    def check(self,context) :
        return True

    def invoke(self,context,event) :
        props = get_props()
        self.active_text = context.space_data.text

        self.info, self.raw = read_info( self.active_text.as_string() )

        self.icon = self.info.get('bl_icon', 'TEXT')
        self.description =  self.info.get('bl_description', 'Some Description')

        props.space = self.info.get('bl_space', 'VIEW3D')
        props.mode = self.info.get('bl_mode', 'ALL')

        self.name = Path(context.space_data.text.name).stem
        self.show_icons = False

        self.add_shelf = False
        self.new_shelf = ""
        self.icons = []

        self.shelf_dirs = get_shelf_dirs()
        self.menus = get_menus()

        CustomShelfProps.shelf_dir = EnumProperty(items = [(str(i), norm_title(i.name), str(i) ) for i in self.shelf_dirs])
        CustomShelfProps.menu = EnumProperty(items = [(str(i), norm_title(i.name), str(i)) for i in self.menus])

        if self.active_text.filepath :
            menu = abspath(bpy.path.abspath(self.active_text.filepath)).parent
            if menu in self.menus :
                props.menus = menu

        return context.window_manager.invoke_props_dialog(self,width = 350)
