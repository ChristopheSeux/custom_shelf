import bpy
from bpy.types import Operator, Panel, PropertyGroup, Menu
from bpy.props import *

from .constants import PACKAGE
from .utils import *
from .properties import CustomShelfProps

from copy import deepcopy
import json


id_type = {
    "Action" : "actions", "Armature":"armatures", "Brush":"brushes",
    "CacheFile":"cache_files", "Camera":"cameras", "Curve":"curves",
    "FreestyleLineStyle":"linestyles", "GreasePencil":"grease_pencil",
    "Group":"groups", "Image":"images", "Key":"shape_keys", "Light":"lights",
    "Lattice":"lattices", "Library":"librairies", "Mask":"masks",
    "Material":"materials", "Mesh":"meshes", "MetaBall":"metaballs",
    "MovieClip":"movieclips", "NodeTree":"node_groups", "Object":"objects",
    "PaintCurve":"paint_curves", "Palette":"palettes", "ParticleSettings":"particles",
    "Scene":"scenes", "Screen":"screens", "Sound":"sounds", "Speaker":"speakers",
    "Text":"texts", "Texture":"textures", "VectorFont":"fonts",
    "WindowManager":"window_managers", "World":"worlds"
}

def dic_to_args(dic):
    args = deepcopy(dic)
    for k, v in dic.items() :
        if k.startswith('bl_') or not isinstance(k, str):
            continue

        if isinstance(v, str) :
            if '/' in v or '\\' in v :
                args[k] = StringProperty(default=v, subtype = "FILE_PATH")
            else :
                args[k] = StringProperty(default=v)

        elif isinstance(v, bool) :
            args[k] = BoolProperty(default=v)

        elif isinstance(v, float) :
            args[k] = FloatProperty(default=v, precision=3)

        elif isinstance(v, int) :
            args[k] = IntProperty(default=v)

        elif isinstance(v,dict) :
            if v.get("items") and isinstance(v["items"],(tuple,list)) :
                args[k] = EnumProperty(items = [(i,i,"") for i in v["items"]])

            elif v.get("collection") and isinstance(v["collection"],str) :
                if v["collection"] not in id_type :
                    print("Collection %s not supported must be in %s"%(v["collection"],id_type))
                else :
                    args[k] = PointerProperty(type = getattr(bpy.types,v["collection"]),poll = v.get('poll'))

    return args


class DrawMenu :
     def __init__(self, bl_idname, label):
         self.bl_idname = bl_idname
         self.label = label

     def __call__(self, cls, context):
        layout = cls.layout
        layout.menu( self.bl_idname, icon='ASSET_MANAGER', text=f' {self.label} ')

class BasicOperator(bpy.types.Operator) :
    def __init__(self, name, function) :
        super().__init__()
        idname = norm_name(name)

        self.bl_label = name
        self.bl_idname = f'{PACKAGE}.{idname}'

        self.function = function

    def execute(self, context) :

        self.function()

        return {'FINISHED'}


class CSHELF_MT_shelf_menu(Menu):
    settings = {}
    scripts = []

    def draw(self,context) :
        layout = self.layout

        for script in self.scripts :
            script_name = norm_title(script['bl_name'])
            layout.operator(operator=script['bl_operator'], text=script_name, icon=script['bl_icon'])


class CSHELF_OT_shelf_popup(Operator) :
    def initialyse(self, label, info) :

        self.bl_label = norm_title(label)
        self.bl_idname = f'{norm_name(__package__)}.{norm_name(label)}'

        self.args = dic_to_args(info)

        Props = type("props",(PropertyGroup,),{'__annotations__' : self.args})
        bpy.utils.register_class(Props)

        self.prop = norm_name(label).strip('_')
        setattr(CustomShelfProps, self.prop, PointerProperty(type = Props))
        self.info = info

        return

    def bl_report(self,*args) :
        if len(args) and args[0] in ('INFO','ERROR') :
            return self.report({args[0]},' '.join(args[1:]))
        else :
            print(*args)

    def invoke(self, context, event):
        if event.alt :
            for t in [t for t in bpy.data.texts if Path(bpy.path.abspath(t.filepath) ) == self.script] :
                bpy.data.texts.remove(t)

            text = bpy.data.texts.load(str(self.script))

            areas = [a for a in context.screen.areas if a.type == 'TEXT_EDITOR']

            if areas :
                text_editor = areas[0]

            else  :
                bpy.ops.screen.area_split(direction = "VERTICAL")

                text_editor = context.screen.areas[-1]
                text_editor.type = "TEXT_EDITOR"
                text_editor.spaces[0].show_syntax_highlight = True
                text_editor.spaces[0].show_word_wrap = True
                text_editor.spaces[0].show_line_numbers = True

                #context_copy = context.copy()
                #context_copy['area'] = text_editor
                #bpy.ops.text.properties(context_copy)
                #bpy.ops.view3d.toolshelf(context_copy)

            text_editor.spaces[0].text = text

            return {"FINISHED"}
        else :
            if self.args :
                #set default value for collection
                props = getattr(get_props(), self.prop)
                for k,v in self.info.items() :
                    if isinstance(v,dict) and v.get('collection') :
                        collection = getattr(bpy.data, id_type[v["collection"]])

                        setattr(props, k, collection.get(v["value"]))

                return context.window_manager.invoke_props_dialog(self)
            else :
                return self.execute(context)



    def execute(self, context):

        info, raw = read_info(self.script.read_text() )

        values = dict(info)
        props = getattr(get_props(), self.prop)
        for arg in self.args :
            if arg.startswith('bl_') : continue
            value = getattr(props, arg)

            if hasattr(value,"name") :
                values[arg] = {'value': value.name}

            elif isinstance(info.get(arg), dict) :
                values[arg] = {'value': value}

            elif isinstance(info.get(arg), bool) :
                values[arg] = int(value)

            else :
                values[arg] = value

        script_info = json.dumps(values, indent=4)
        script = f'info = {script_info}\n\n{raw}'
        exec(compile(script, '', 'exec'), {'info' : values, 'print': self.bl_report})

        return {'FINISHED'}


    def draw(self,context) :
        layout = self.layout

        props = getattr(get_props(), self.prop)
        for k in self.args :
            if k.startswith('bl_') : continue

            row = layout.row(align = True)
            row.label(text=norm_title(k))

            row.prop(props, k, text='')


    def check(self,context) :
        return True
