
from pathlib import Path
from os.path import exists
import json

from .utils import *
from .properties import CustomShelfProps
from .Types import *


def get_shelf_dirs():
    shelf_dirs = [d.path for d in get_prefs().shelf_dirs] +  [ get_prefs().shelf_dir ]
    shelf_dirs = [Path(d) for d in shelf_dirs if d ]
    return [ d for d in shelf_dirs if d.exists() ]


def get_menus():
    menus = []
    for d in get_shelf_dirs() :
        menus+= [i for i in d.iterdir() if not i.name.startswith('.') ]
    return menus



def read_shelves() :
    # unregister menus :
    for menu_infos in CustomShelfProps.menu_list :
        try :
            bpy.utils.unregister_class(menu_infos['menu'])
        except :
            pass

        for i, f in enumerate(menu_infos['space'].draw._draw_funcs) :

            if isinstance(f , DrawMenu) :
                print(f.__class__, type(f))
                print('\n', 'Removing Menu', i)
                del menu_infos['space'].draw._draw_funcs[i]


        #menu_infos['space'].remove(menu_infos['draw_menu'])

        #CustomShelfProps.menu_list.remove(menu_infos)
    CustomShelfProps.menu_list = []

    shelf_scripts = []
    for d in get_shelf_dirs() :
        shelf_scripts+=list(Path(d).glob('*/*.py'))


    spaces = {}
    for script in shelf_scripts :
        script_name = script.stem

        info, lines = read_info(script.read_text() )

        info = {**dict(bl_description='', bl_space='TOPBAR', bl_icon='WORDWRAP_OFF',
        bl_mode='ALL', bl_menu=script.parent.name ), **info}

        #print('### Info', info)

        popup_args = {'script': script, 'bl_description': info['bl_description'] }
        popup = type(script_name, (CSHELF_OT_shelf_popup,), popup_args)
        popup.initialyse( popup, script_name, info )
        bpy.utils.register_class(popup)

        space = info['bl_space']
        if space not in spaces :
            spaces[space] = {}

        menu = info['bl_menu']
        if menu not in spaces[space] :
            spaces[space][menu] = []

        spaces[space][menu].append({'bl_name':script_name, 'bl_operator' : popup.bl_idname,**info})
        #scripts.append()

    #print('\n',json.dumps(spaces, indent=4),'\n')

    for space, menus in spaces.items() :
        #if space == 'TOPBAR' : continue

        space_type = getattr(bpy.types, space+'_MT_editor_menus')
        print('space_type', space_type)

        for menu_name, scripts in menus.items() :
            menu_name = menu_name.replace('_',' ').title()
            bl_idname = f'{__package__.upper()}_MT_{menu_name}'

            menu_args = {
                'bl_idname' : bl_idname,
                'bl_label' : menu_name,
                'menu' : menu_name,
            }

            #if menu_name == 'Scene' :
            print('bl_idname', bl_idname)

            menu = type(menu_name.lower(), (CSHELF_MT_shelf_menu,), menu_args)
            menu.scripts = sorted(scripts, key=lambda x :x['bl_name'])

            bpy.utils.register_class(menu)

            draw_menu = DrawMenu(bl_idname, menu_name)
            space_type.append(draw_menu)

            CustomShelfProps.menu_list.append( {'menu': menu, 'draw_menu': draw_menu, 'space' : space_type} )

            #print(CustomShelfProps.menu_list)
