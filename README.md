# custom_shelf

```
info = {
    'icon' : 'SORTALPHA',
    'description' : 'Rename active object in something',
    'name' : 'Some name',
    'enum' : {'value': '3', 'items': ['1', '2', '3', '4']},
    'int' : 2,
    'path' : '/temp',
    'bool' : False,
    'float' : 2.0,
    'collection' : {'value': 'object', 'collection': 'Object'},
}
```


Dictionnary to put at the beginning of your code to have tooltip, icon 
and variables inside a popup

To get yout variables in your code :

name = info['name']

The collection return the name, so to get an object :

ob = bpy.data.objects.get(info['collection']['value'])


You can put a json file named "settings.json" inside your panel folder for tagging the folder
{"tags" : ["td"]}

You can next filter by tags by clicking on the arrow icon

Script file starting with '_' are hided by default, you can unhide them by unchecking the filter icon


### Updates:

- v0.0.3 2020-02-16:
    Clean fix : Fix warning for panels name and annotation type

- v0.0.2 2019-12-04:
    Update to 2.81