import bpy


class CSHELF_MT_text_editor(bpy.types.Menu):
    bl_label = "Shelf"
    bl_idname = "CSHELF_MT_text_editor"

    def draw(self, context):
        layout = self.layout

        layout.operator("customshelf.add_script", text='Add Script',
        emboss=True, icon= "LONGDISPLAY")
