import bpy
from bpy.props import StringProperty, CollectionProperty, EnumProperty, PointerProperty,\
BoolProperty


class ModeProps(bpy.types.PropertyGroup) :
    TOPBAR : EnumProperty(items= [(i, i, '') for i in ('ALL',)] )
    VIEW3D : EnumProperty(items= [(i, i, '') for i in ('ALL', 'OBJECT', 'POSE',
    'EDIT', 'SCUPLT', 'WEIGHT_PAINT', 'TEXTURE_PAINT', 'PARTICLES_EDIT') ])
    NODE : EnumProperty(items= [(i, i, '') for i in ('ALL',)] )
    IMAGE : EnumProperty(items= [(i, i, '') for i in ('ALL',)] )
    GRAPH : EnumProperty(items= [(i, i, '') for i in ('ALL',)] )
    DOPESHEET : EnumProperty(items= [(i, i, '') for i in ('ALL',)] )

class CustomShelfProps(bpy.types.PropertyGroup) :
    menu_list = []
    space : EnumProperty(items=[(i,i,'') for i in ('TOPBAR', 'VIEW3D', 'NODE',
    'IMAGE', 'GRAPH', 'DOPESHEET') ])

    modes : PointerProperty(type=ModeProps)
    icon : StringProperty(default='TEXT')
    show_icons : BoolProperty(default=False)


class ShelveDirProps(bpy.types.PropertyGroup) :
    path : StringProperty(name="Path", subtype='FILE_PATH')


class CustomShelfPrefs(bpy.types.AddonPreferences):
    bl_idname = __package__

    shelf_dir : StringProperty(name="Path", subtype='FILE_PATH')
    shelf_dirs : CollectionProperty(type = ShelveDirProps)

    def draw(self, context):
        prefs = bpy.context.preferences.addons[__package__].preferences
        layout = self.layout

        row = layout.row(align = True)
        row.label(text='Shelf Folders :')
        row.operator('customshelf.add_shelf_dir', icon='ADD', text='')

        col = layout.column(align = True)
        row = col.row(align=True)
        row.prop(self, 'shelf_dir', text='')
        row = row.row(align=True)
        row.enabled = False
        op = row.operator('customshelf.remove_shelf_dir', icon='REMOVE', text='')

        for i, shelf in enumerate(prefs.shelf_dirs) :
            row = col.row(align=True)
            row.prop(shelf, 'path', text='')
            row.operator('customshelf.remove_shelf_dir', icon='REMOVE', text='').index = i
