from pathlib import Path
import re
import bpy

def get_prefs() :
    return bpy.context.preferences.addons[__package__].preferences

def get_props() :
    return bpy.context.window_manager.custom_shelf

def norm_name(name):
    return name.replace(' ','_').replace('-','_').lower()

def norm_title(name):
    return name.replace('_',' ').replace('-',' ').title()

def read_info(script) :
    expr = re.compile(r'info[ ]+=[ ]+(\{[\w\'\"\n:\/.\]\[,\}\{ ]+\})')

    res = expr.search(script)
    if not res :
        print('The info header in the file cannot be read')
        print(script[:300])

        return {}, script

    info = eval(res.groups()[0])
    raw = script.replace(res.group(), '', 1).strip('\n')

    return info, raw
